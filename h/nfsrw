/* Copyright 1996 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* nfs read write
 *
 * 2/9/92
 * by J Sutton
 */

#ifndef __nfsrw_h
#define __nfsrw_h

/*
        Destination address is passed into this routine. count can be > valid memory.
        Uses clntudp coroutine mechanism to get many simultaneous transfers
*/
extern _kernel_oserror *nfsrw_readmany
(
        int *readres_len,
        char *readres_data,
        MountPoint *mount_point,
        RO_nfs_fh3 *fhandle,
        unsigned long offset,
        int count,
        fattr3 *nattr
);

extern _kernel_oserror *nfsrw_writemany
(
        fattr3 **nattr,
        MountPoint *mount_point,
        RO_nfs_fh3 *fhandle,
        unsigned long offset,
        char *data,
        int count
);

#endif
