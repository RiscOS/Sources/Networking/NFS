/* Copyright 1996 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __NFSNFS_H
#include "nfsheaders.h"
#endif

_kernel_oserror *fsentry_getbytes( FSEntry_GetBytes_Parameter *parm )
{
        _kernel_oserror *err;
        OpenFile *of = (OpenFile *)(parm->getbytes_definition.handle);

        hourglass_on();

#ifdef SHOWCALLS
        printf(
                "_GetBytes( %s, %d, %#010x, %d )\n",
                of->nfs_fd.desc.name,
                parm->getbytes_definition.file_offset_to_get_data_from,
                (int)parm->getbytes_definition.destination_start,
                parm->getbytes_definition.bytes_to_read );
#endif

        err = getbytes(
                &parm->getbytes_definition.destination_start,
                &of->nfs_fd,
                &parm->getbytes_definition.file_offset_to_get_data_from,
                &parm->getbytes_definition.bytes_to_read );

        hourglass_off();

        return err;
}
