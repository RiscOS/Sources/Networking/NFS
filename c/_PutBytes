/* Copyright 1996 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __NFSNFS_H
#include "nfsheaders.h"
#endif

_kernel_oserror *fsentry_putbytes( FSEntry_PutBytes_Parameter *parm )
{
        OpenFile *of = (OpenFile *)(parm->handle);
        _kernel_oserror *err;

        hourglass_on();

#ifdef SHOWCALLS
        printf(
                "_PutBytes( %s, %d, %#010x, %d )\n",
                of->nfs_fd.desc.name,
                parm->file_offset_to_put_data_to,
                (int)parm->source_start,
                parm->bytes_to_write );
#endif

        err = putbytes(
                &parm->source_start,
                &of->nfs_fd,
                &parm->file_offset_to_put_data_to,
                &parm->bytes_to_write );

        hourglass_off();

        return err;
}
